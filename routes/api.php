<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test-djikstra', 'api\TestDjikstraController@index');
Route::get('/test-bellman-ford', 'api\TestDjikstraController@bellman');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
  'as' => 'auth.',
  'prefix' => 'auth'
], function () {
  Route::post('login', 'Api\AdminAuthController@login');
});

Route::group([
  'as' => 'admin.',
  'prefix' => 'admin',
  'middleware' => 'auth:api'
], function () {
    Route::apiResource('category', 'Api\CategoryController');

    Route::get('destination/category', 'Api\DestinationController@category');
    Route::apiResource('destination', 'Api\DestinationController');
});
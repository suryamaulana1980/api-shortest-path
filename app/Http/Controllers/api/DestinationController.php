<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Destination;
use App\Models\Category;

class DestinationController extends Controller
{
    public function index()
    {
        $destination = Destination::paginate(10);

        return $destination;
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'id_category' => 'required',
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $destination = Destination::create($validated + [
            'id_user' => auth()->user()->id
        ]);

        return response([
            'message' => 'berhasil input detinasi',
        ], 201);
    }

    public function show(Destination $destination)
    {
        return $destination;
    }

    public function update(Request $request, Destination $destination)
    {
        $validated = $this->validate($request, [
            'id_category' => 'required',
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $destination->update($validated + [
            'id_user' => auth()->user()->id
        ]);

        return response([
            'message' => 'berhasil update detinasi',
        ], 201);
    }

    public function category()
    {
        $category = Category::get();
        return $category;
    }
}

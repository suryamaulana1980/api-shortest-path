<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SplPriorityQueue;
use SplStack;

class TestDjikstraController extends Controller
{
    public function index()
    {
        $graph = [
            'A' => ['B' => 3, 'C' => 5, 'D' => 9],
            'B' => ['A' => 3, 'C' => 3, 'D' => 4, 'E' => 7],
            'C' => ['A' => 5, 'B' => 3, 'D' => 2, 'E' => 6, 'F' => 3],
            'D' => ['A' => 9, 'B' => 4, 'C' => 2, 'E' => 2, 'F' => 2],
            'E' => ['B' => 7, 'C' => 6, 'D' => 2, 'F' => 5],
            'F' => ['C' => 3, 'D' => 2, 'E' => 5],
        ];
        
        $source = "F";
        $target = "E";
        
        $result = $this->dijkstra($graph, $source, $target);
        extract($result);
        
        echo "Distance from $source to $target is $distance \n";
        echo "Path to follow : ";
        
        while (!$path->isEmpty()) {
            echo $path->pop() . "\t";
        }
    }

    function dijkstra(array $graph, string $source, string $target): array {
        $dist = [];
        $pred = [];
        $Queue = new SplPriorityQueue();

        foreach ($graph as $v => $adj) {

            $dist[$v] = PHP_INT_MAX;
            $pred[$v] = null;
            foreach ($adj as $w => $cost) {
                $Queue->insert($w, $cost);
            }
        }
        $dist[$source] = 0;
    
        while (!$Queue->isEmpty()) {
            $u = $Queue->extract();
            if (!empty($graph[$u])) {
                foreach ($graph[$u] as $v => $cost) {
                    if ($dist[$u] + $cost < $dist[$v]) {
                        $dist[$v] = $dist[$u] + $cost;
                        $pred[$v] = $u;
                    }
                }
            }
        }
    
        $S = new SplStack();
        $u = $target;
        $distance = 0;
    
        while (isset($pred[$u]) && $pred[$u]) {
            $S->push($u);
            $distance += $graph[$u][$pred[$u]];
            $u = $pred[$u];
        }
    
        if ($S->isEmpty()) {
            return ["distance" => 0, "path" => $S];
        } else {
            $S->push($source);
            return ["distance" => $distance, "path" => $S];
        }
    }

    function bellmanFord(array $graph, int $source): array {
        $dist = [];
        $len = count($graph);
    
        foreach ($graph as $v => $adj) {
        $dist[$v] = PHP_INT_MAX;
        }
    
        $dist[$source] = 0;
    
        for ($k = 0; $k < $len - 1; $k++) {
        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j < $len; $j++) {
            if ($dist[$i] > $dist[$j] + $graph[$j][$i]) {
                $dist[$i] = $dist[$j] + $graph[$j][$i];
            }
            }
        }
        }
    
        for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len; $j++) {
            if ($dist[$i] > $dist[$j] + $graph[$j][$i]) {
            echo 'The graph contains a negative-weight cycle!';
            return [];
            }
        }
        }
        return $dist;
    }

    public function bellman()
    {
        define("I", PHP_INT_MAX);

        $graph = [
            0 => [I, 3, 5, 9, I, I],
            1 => [3, I, 3, 4, 7, I],
            2 => [5, 3, I, 2, 6, 3],
            3 => [9, 4, 2, I, 2, 2],
            4 => [I, 7, 6, 2, I, 5],
            5 => [I, I, 3, 2, 5, I]
        ];

        // return $graph;

        $matrix = array(
            0 => array(0, 3, 4),
            1 => array(0, 0, 2),
            2 => array(0, -2, 0),
        );



        $source = 1;

        $distances = $this->bellmanFord($graph, $source);

        foreach($distances as $target => $distance) {
            echo "distance from $source to $target is $distance \n";
        }
    }
}

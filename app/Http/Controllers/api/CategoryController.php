<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::paginate(10);
        return $category;
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $category = Category::create($validated + [
            'id_user' => auth()->user()->id
        ]);

        return response([
            'message' => 'berhasil input kategori',
        ], 201);
    }

    public function update(Request $request, Category $category)
    {
        $validated = $this->validate($request, [
            'name' => 'sometimes',
            'description' => 'sometimes',
        ]);

        $category->update($validated + [
            'id_user' => auth()->user()->id
        ]);

        return response([
            'message' => 'berhasil update kategori',
        ], 201);
    }

    public function delete(Category $category)
    {
        return $category;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'Destination';

    protected $fillable = [
        'id_user','id_category','name','description','address','latitude','longitude'
    ];

    public function user()
    {
        return $this->hasOne('App\Users', 'id_user');
    }

    public function category()
    {
        return $this->hasOne('App\Models\category','id_category');
    }
}

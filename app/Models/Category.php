<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'id_user','name','description'
    ];

    public function user()
    {
        return $this->hasOne('App\Users', 'id_user');
    }

    public function destination()
    {
        return $this->belongsTo('App\Models\Destination', 'id_category');
    }
}
